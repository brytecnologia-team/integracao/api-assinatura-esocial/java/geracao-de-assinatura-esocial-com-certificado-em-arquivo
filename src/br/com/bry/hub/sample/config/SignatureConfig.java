package br.com.bry.hub.sample.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SignatureConfig {

	private static final String PATH_SEPARATOR = File.separator;
	
	private static final String RESOURCES_FOLDER = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR;
	
	public static final String OUTPUT_RESOURCE_FOLDER = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "generatedSignatures" + PATH_SEPARATOR;
	
	public static final List<String> ORIGINAL_DOCUMENTS_LOCATION = new ArrayList<>();
	
	static {
		ORIGINAL_DOCUMENTS_LOCATION.add(RESOURCES_FOLDER + "documents" + PATH_SEPARATOR + "ExemploESocial.xml");
		ORIGINAL_DOCUMENTS_LOCATION.add(RESOURCES_FOLDER + "documents" + PATH_SEPARATOR + "ExemploESocial.xml");
	}
	
}
