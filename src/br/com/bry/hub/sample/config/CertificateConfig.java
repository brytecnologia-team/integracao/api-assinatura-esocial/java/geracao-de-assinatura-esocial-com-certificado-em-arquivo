package br.com.bry.hub.sample.config;

import java.io.File;

public class CertificateConfig {
	
	private static final String PATH_SEPARATOR = File.separator;

	// Certificate folder inside this project.
	// Can be used to store local private key reference.
	private static final String RESOURCES_FOLDER = System.getProperty("user.dir") + "" + PATH_SEPARATOR +  "resources" + PATH_SEPARATOR;

	// If you don't have one, follow the instructions in README.md on how to obtain your personal private key.
	//public static final String PRIVATE_KEY_LOCATION = "<LOCATION_OF_PRIVATE_KEY_FILE>";
	public static final String PRIVATE_KEY_LOCATION = "/home/marcelo.vendramin/Documentos/mev.p12";
	
	//public static final String PRIVATE_KEY_PASSWORD = "<PRIVATE_KEY_PASSWORD>";
	public static final String PRIVATE_KEY_PASSWORD = "Marcelomf";
	
}
