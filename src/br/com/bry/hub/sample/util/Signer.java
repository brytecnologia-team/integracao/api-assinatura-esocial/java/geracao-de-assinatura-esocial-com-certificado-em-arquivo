package br.com.bry.hub.sample.util;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

public class Signer {

	private PrivateKey privateKey;
	private byte[] certificate;

	public Signer(String keyFilePath, String keyPassword) throws IOException {
		if (Security.getProvider("BC") == null)
			Security.addProvider(new BouncyCastleProvider());

		InputStream is = new FileInputStream(keyFilePath);
		KeyStore ks = loadPkcs12KeyStore(keyPassword, is);

		this.privateKey = loadPrivateKey(keyPassword, ks);
		this.certificate = loadCertificateContent(ks);
	}

	public byte[] getCertificate() {
		return certificate;
	}

	public byte[] sign(byte[] data) throws IOException {
		if (data == null) {
			throw new IOException("Data to sign must not be null.");
		}

		Signature signature = null;
		try {
			signature = Signature.getInstance("SHA256withRSA", "BC");
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
			throw new IOException("No such algorithm or provider found.", e);
		}

		try {
			signature.initSign(this.privateKey);
			signature.update(data);
			return signature.sign();
		} catch (Exception e) {
			throw new IOException("Error during signature operation.", e);
		}

	}

	private byte[] loadCertificateContent(KeyStore ks) throws IOException {
		X509Certificate x509Certificate = null;
		try {
			x509Certificate = (X509Certificate) ks.getCertificate(ks.aliases().nextElement());
		} catch (KeyStoreException e) {
			throw new IOException("Error while listing key stores certificate aliases.", e);
		}

		try {
			return x509Certificate.getEncoded();
		} catch (CertificateEncodingException e) {
			throw new IOException(e.getMessage(), e);
		}
	}

	private PrivateKey loadPrivateKey(String keyPassword, KeyStore ks) throws IOException {
		try {
			return (PrivateKey) ks.getKey(ks.aliases().nextElement(), keyPassword.toCharArray());
		} catch (Exception e) {
			throw new IOException("Error while obtaining the private key from pkcs12 keyStore.", e);
		}
	}

	private KeyStore loadPkcs12KeyStore(String keyPassword, InputStream is) throws IOException {
		KeyStore ks = null;
		try {
			ks = KeyStore.getInstance("pkcs12");
			ks.load(is, keyPassword.toCharArray());
		} catch (Exception e) {
			throw new IOException("Error while loading the private key from pkcs12 keyStore.", e);
		}
		return ks;
	}

}
