/**
 * 
 */
package br.com.bry.hub.sample.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ConverterUtil {
	
	public static String convertObjectToJSON(Object object) {
		
		final ObjectMapper mapper = new ObjectMapper();
		
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
